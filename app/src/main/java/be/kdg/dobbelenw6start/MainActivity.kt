package be.kdg.dobbelenw6start

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.util.Log
import android.util.SparseArray
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import be.kdg.dobbelenw6start.databinding.ActivityMainBinding
import java.util.*

const val MAX_TURNS = 5
private const val SCORE = "SCORE"

class MainActivity : AppCompatActivity() {
    private var score = 0
    private var turns = 0
    private var die1Count = 1
    private var die2Count = 1
    private lateinit var binding: ActivityMainBinding
    private val drawables = SparseArray<Drawable>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
      Log.i("mainactivity", "onCreate: ")
      binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        for (i in 1..6) {
            val id: Int = resources.getIdentifier("die$i", "drawable", packageName)
            drawables.append(i, ContextCompat.getDrawable(this, id))
        }
        addEventhandlers()
        updateView()
    }


  override fun onSaveInstanceState(outState: Bundle) {
    outState.putInt(SCORE,score)
    super.onSaveInstanceState(outState)
  }

  override fun onRestoreInstanceState(savedInstanceState: Bundle) {
    super.onRestoreInstanceState(savedInstanceState)
    score=savedInstanceState.getInt(SCORE)
    updateView()
  }

  private fun addEventhandlers(){
        binding.rollDice.setOnClickListener {
            if (turns < MAX_TURNS) {
                val random = Random()
                die1Count = random.nextInt(6) + 1
                die2Count = random.nextInt(6) + 1
                score += die1Count + die2Count
            }
            turns++
            updateView()
        }
        binding.btnRestart.setOnClickListener {
            turns = 0
            die2Count = 1
            die1Count = 1
            score = 0
            updateView()
        }
    }

    private fun updateView(){
        binding.apply {
            rollDice.isEnabled = turns < MAX_TURNS
            tvTurns.text = resources.getString(R.string.turns, turns)
            tvScore.text = resources.getString(R.string.score, score)
            leftDie.setImageDrawable(drawables.get(die1Count))
            rightDie.setImageDrawable(drawables.get(die2Count))
        }
    }

  override fun onRestart() {
    super.onRestart()
    Log.i("mainactivity", "onRestart: ")
  }

  override fun onStop() {
    super.onStop()
    Log.i("mainactivity", "onStop: ")

  }

  override fun onPause() {
    super.onPause()
    Log.i("mainactivity", "onPause: ")

  }

  override fun onDestroy() {
    super.onDestroy()
    Log.i("mainactivity", "onDestroy: ")
  }

  override fun onResume() {
    super.onResume()
    Log.i("mainactivity", "onResume: ")

  }
}